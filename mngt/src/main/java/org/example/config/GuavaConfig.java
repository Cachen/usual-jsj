package org.example.config;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.Duration;

/**
 * guava缓存
 */
@Configuration
public class GuavaConfig {

    @Bean("guavaCache")
    public Cache<String,Object> CacheBuilder(){
        return CacheBuilder.newBuilder()
                //设置cache的初始大小为10，要合理设置该值
                .initialCapacity(10)
                //设置最大容量
                .maximumSize(500)
                //设置并发级别为cpu核心数
                .concurrencyLevel(Runtime.getRuntime().availableProcessors())
                //设置cache中的数据在写入之后的存活时间为10分钟
//                .expireAfterWrite(10, TimeUnit.MINUTES)
                //相对时间 每次访问后重新刷新该缓存的过期时间
                .expireAfterAccess(Duration.ofMinutes(10))
                //构建cache实例
                .build();
    }
}
