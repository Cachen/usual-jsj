package org.example.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.example.model.UserDO;

public interface UserMapper extends BaseMapper<UserDO> {
}
