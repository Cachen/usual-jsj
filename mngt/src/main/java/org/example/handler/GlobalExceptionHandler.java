package org.example.handler;

import org.example.common.enums.ResultEnum;
import org.example.web.resp.R;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * 全局异常拦截
 * 如果异常是在filter中，捕获不到。自定义ExceptionFilter  再转发到处理的controllerrequest.setAttribute("filter.error", e);
 * request.getRequestDispatcher("/error/exthrow").forward(request, response);
 */
@ControllerAdvice
public class GlobalExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);


    /**
     * 验证异常
     *
     * @param req
     * @param e
     * @return
     * @throws MethodArgumentNotValidException
     */
    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    @ResponseBody
    public R handleMethodArgumentNotValidException(HttpServletRequest req, MethodArgumentNotValidException e)  {

        BindingResult bindingResult = e.getBindingResult();
        StringBuilder errorMesssage = new StringBuilder();
        errorMesssage.append("Invalid Request:\n");

        for (FieldError fieldError : bindingResult.getFieldErrors()) {
            errorMesssage.append(fieldError.getDefaultMessage()).append("\n");
        }
        logger.warn("MethodArgumentNotValidException:{}", errorMesssage);

        return R.error(ResultEnum.ParamErrResult.getCode(), errorMesssage.toString());
    }


    /**
     * 无权限
     *
     * @param req
     * @param e
     * @return
     * @throws Exception
     */
    @ExceptionHandler(value = AccessDeniedException.class)
    @ResponseBody
    public R handleAccessDeniedException(HttpServletRequest req, AccessDeniedException e) {
        logger.error(e.getMessage(), e);
        return R.error(ResultEnum.AccessDeniedResult.getCode(), ResultEnum.AccessDeniedResult.getMsg());
    }

    /**
     * 未知异常
     *
     * @param req
     * @param e
     * @return
     * @throws Exception
     */
    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public R handleException(HttpServletRequest req, Exception e) {

        logger.error(e.getMessage(), e);
        return R.error(ResultEnum.FailResult.getCode(), ResultEnum.FailResult.getMsg());
    }

    /**
     * NullPointerException
     *
     * @param req
     * @param e
     * @return
     * @throws Exception
     */
    @ExceptionHandler(value = NullPointerException.class)
    @ResponseBody
    public R handleNullPointerException(HttpServletRequest req, Exception e) {

        logger.error(e.getMessage(), e);
        return R.error(ResultEnum.FailResult.getCode(), ResultEnum.FailResult.getMsg());
    }

    /**
     * RuntimeException
     *
     * @param req
     * @param e
     * @return
     * @throws Exception
     */
    @ExceptionHandler(value = RuntimeException.class)
    @ResponseBody
    public R handleRuntimeException(HttpServletRequest req, Exception e) {

        logger.error(e.getMessage(), e);
        return R.error(ResultEnum.FailResult.getCode(), ResultEnum.FailResult.getMsg());
    }

    /**
     * ClassCastException
     *
     * @param req
     * @param e
     * @return
     * @throws Exception
     */
    @ExceptionHandler(value = ClassCastException.class)
    @ResponseBody
    public R handleClassCastException(HttpServletRequest req, Exception e) {

        logger.error(e.getMessage(), e);
        return R.error(ResultEnum.FailResult.getCode(), ResultEnum.FailResult.getMsg());
    }

    /**
     * IndexOutOfBoundsException
     *
     * @param req
     * @param e
     * @return
     * @throws Exception
     */
    @ExceptionHandler(value = IndexOutOfBoundsException.class)
    @ResponseBody
    public R handleIndexOutOfBoundsException(HttpServletRequest req, Exception e) {

        logger.error(e.getMessage(), e);
        return R.error(ResultEnum.FailResult.getCode(), ResultEnum.FailResult.getMsg());
    }

    /**
     * StackOverflowError
     *
     * @param req
     * @param e
     * @return
     * @throws Exception
     */
    @ExceptionHandler(value = StackOverflowError.class)
    @ResponseBody
    public R handleStackOverflowError(HttpServletRequest req, Exception e) {

        logger.error(e.getMessage(), e);
        return R.error(ResultEnum.FailResult.getCode(), ResultEnum.FailResult.getMsg());
    }

    /**
     * ClassNotFoundException
     *
     * @param req
     * @param e
     * @return
     * @throws Exception
     */
    @ExceptionHandler(value = ClassNotFoundException.class)
    @ResponseBody
    public R handleClassNotFoundException(HttpServletRequest req, Exception e) {

        logger.error(e.getMessage(), e);
        return R.error(ResultEnum.FailResult.getCode(), ResultEnum.FailResult.getMsg());
    }

}
