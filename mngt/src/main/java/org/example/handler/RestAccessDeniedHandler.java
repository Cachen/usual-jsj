//package org.example.handler;
//
//import com.alibaba.fastjson.JSON;
//import org.example.common.enums.ResultEnum;
//import org.example.web.resp.R;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.MediaType;
//import org.springframework.security.access.AccessDeniedException;
//import org.springframework.security.web.access.AccessDeniedHandler;
//import org.springframework.stereotype.Component;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//import java.io.PrintWriter;
//
//
///**
// * 异常过滤器自己new了一个AccessDeniedHandler，用不到了
// */
//public class RestAccessDeniedHandler implements AccessDeniedHandler {
//    @Override
//    public void handle(HttpServletRequest request,
//                       HttpServletResponse response,
//                       AccessDeniedException e) throws IOException {
//
//        response.setStatus(HttpStatus.FORBIDDEN.value());
//        response.setContentType(MediaType.APPLICATION_JSON_UTF8_VALUE);
//        R error = R.error(ResultEnum.AccessDeniedResult.getCode(), ResultEnum.AccessDeniedResult.getMsg());
//
//        try(PrintWriter out = response.getWriter()) {
//            out.write(JSON.toJSONString(error));
//            out.flush();
//        }
//
//    }
//}
