package org.example.services.impl;

import org.example.model.UserDO;
import org.example.model.dto.UserDetailDTO;
import org.example.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service("weChatUserServiceDetailService")
public class WeChatUserServiceDetailService  implements UserDetailsService {

    private static final Logger logger = LoggerFactory.getLogger(WeChatUserServiceDetailService.class);

    @Autowired
    private UserService userService;

    @Override
    public UserDetails loadUserByUsername(String openid) throws UsernameNotFoundException {
        UserDO user= userService.getOneByOpenid(openid);
        if(user==null){
            logger.error("找不到用户：{}",openid);
            throw new UsernameNotFoundException("用户" + openid + "不存在!");
        }

        // TODO 加载用户的权限

        UserDetailDTO userDetailDTO = new UserDetailDTO(user.getUserId(),
                user.getUsername(),
                user.getPassword(),
                user.getHeadImage(),
                user.getSuperMan(),
                new ArrayList<>());
        return userDetailDTO;
    }
}
