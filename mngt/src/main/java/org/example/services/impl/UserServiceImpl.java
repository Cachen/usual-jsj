package org.example.services.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.RequiredArgsConstructor;
import org.example.common.enums.BOField;
import org.example.dao.UserMapper;
import org.example.model.UserDO;
import org.example.services.UserService;
import org.example.util.IdWorker;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserMapper userMapper;

    @Override
    public UserDO getOneByPhone(String mobile) {
        QueryWrapper<UserDO> eq = new QueryWrapper<UserDO>().eq(BOField.MOBILE, mobile);
        UserDO userDO = userMapper.selectOne(eq);
        return userDO;
    }

    @Override
    public UserDO getOneByUsername(String username) {
        QueryWrapper<UserDO> eq = new QueryWrapper<UserDO>().eq(BOField.USERNAME, username);
        UserDO userDO = userMapper.selectOne(eq);
        return userDO;
    }

    @Override
    public UserDO getOneByOpenid(String openid) {
        QueryWrapper<UserDO> eq = new QueryWrapper<UserDO>().eq(BOField.OPENID, openid);
        UserDO userDO = userMapper.selectOne(eq);
        return userDO;
    }

    @Override
    public int save(UserDO userDO) {
        long id = IdWorker.getId();
        userDO.setUserId(id);
        userDO.setEnable(true);
        return userMapper.insert(userDO);
    }
}
