package org.example.services.impl;

import org.example.model.UserDO;
import org.example.model.dto.UserDetailDTO;
import org.example.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service("phoneUserServiceDetailService")
public class PhoneUserServiceDetailService implements UserDetailsService {

    private static final Logger logger = LoggerFactory.getLogger(PhoneUserServiceDetailService.class);

    @Autowired
    private UserService userService;

    @Override
    public UserDetails loadUserByUsername(String phone) throws UsernameNotFoundException {
        UserDO oneByPhone = userService.getOneByPhone(phone);
        if(oneByPhone==null){
            logger.error("找不到用户：{}",phone);
            throw new UsernameNotFoundException("用户" + phone + "不存在!");
        }

        // TODO 加载用户的权限

        UserDetailDTO userDetailDTO = new UserDetailDTO(oneByPhone.getUserId(),
                oneByPhone.getUsername(),
                "",
                oneByPhone.getHeadImage(),
                oneByPhone.getSuperMan(),
                new ArrayList<>());
        return userDetailDTO;
    }
}
