package org.example.services;

import org.example.model.UserDO;

public interface UserService {
    UserDO getOneByPhone(String mobile);

    UserDO getOneByUsername(String username);

    int save(UserDO userDO);

    UserDO getOneByOpenid(String openid);
}
