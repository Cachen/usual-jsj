package org.example.model;


import com.baomidou.mybatisplus.annotation.TableName;
import lombok.*;
import org.example.model.base.Entity;

@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("user")
@ToString
@EqualsAndHashCode(callSuper = true)
public class UserDO extends Entity {

    private Long userId;

    private String username;

    private String password;

    private String mobile;

    private Boolean enable;//是否激活 1激活 0 未激活

    private Integer source;

    private String wxOpenid;//微信openid

    private String headImage;//头像

    private Integer superMan;//超级管理员,就是个标识

//    @TableField(exist = false)
//    private Set<RoleDO> roleSet;
}
