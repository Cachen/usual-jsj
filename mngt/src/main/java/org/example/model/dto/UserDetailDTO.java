package org.example.model.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class UserDetailDTO extends User {

    private Long userId;

    private String username;

    private String password;

    private String headImage;

    private Integer superMan;

//    private String mobile;

//    private String email;

//    private String openId;

    public UserDetailDTO(Long userId, String username, String password, String headImage, Integer superMan, Collection<? extends GrantedAuthority> authorities){
        super(username, password, authorities);
        this.userId=userId;
        this.username=username;
        this.password=password;
        this.headImage=headImage;
        this.superMan=superMan;
    }
}
