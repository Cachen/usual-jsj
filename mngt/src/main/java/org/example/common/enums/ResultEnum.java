package org.example.common.enums;

public enum ResultEnum {

    SuccessResult(1,"请求成功"),

    LoginFailResult(401,"登录失败"),
    InvalidTokenResult(402,"未登录或token已失效"),
    AccessDeniedResult(403,"无权限"),

    ParamErrResult(510,"参数错误"),
    ExistResult(520,"已存在"),
    FailResult(530,"操作失败，服务器发生了错误");

    ResultEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    private Integer code;
    private String msg;

    public Integer getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
