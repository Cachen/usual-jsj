package org.example.common.enums;

public interface BOField {

    String MOBILE = "mobile";
    String USERNAME = "username";
    String OPENID = "openid";
}
