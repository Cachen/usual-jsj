package org.example.web.req;

import lombok.Data;

@Data
public class RegisterREQ {

    private String username;

    private String mobile;

    private String code;
}
