package org.example.web.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author ccx
 * @version V1.0
 * @Package org.example.web.controller
 * @date 2020/4/10 23:53
 */
@RestController
@RequestMapping("per")
public class PermissionController {

    @PreAuthorize("@pms.hasPermission('admin')")
    @RequestMapping(value = "test1",method = RequestMethod.GET)
    public String permissionTest1(){
        return "success";
    }

    @RequestMapping(value = "test2",method = RequestMethod.GET)
    public String permissionTest2(){
        return "permissionTest2";
    }
}
