package org.example.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("test")
public class TestController {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @RequestMapping(value = "getEncodePW",method = RequestMethod.GET)
    public String getEncodePW(String pw){
        return passwordEncoder.encode(pw);
    }

}
