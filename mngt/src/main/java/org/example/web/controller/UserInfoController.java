package org.example.web.controller;

import org.example.model.UserDO;
import org.example.services.UserService;
import org.example.util.JwtUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user/info")
public class UserInfoController {

    @Autowired
    private UserService userService;
    @Autowired
    private JwtUtils jwtUtils;

    @RequestMapping(value = "info",method = RequestMethod.POST)
    public UserDO getUserInfo(@RequestHeader("Authorization")String token){
        String username = jwtUtils.getClaims(token).getSubject();
        return userService.getOneByUsername(username);
    }

}
